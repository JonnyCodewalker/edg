use std::process::Command;
use std::str;

fn main() {
    let cmd = Command::new("sh")
        .args(&["-c", "eopkg li"])
        .output()
        .expect("failed to execute process");
    if cmd.status.success() {
        for line in str::from_utf8(&cmd.stdout).unwrap_or_default().lines() {
            let package = line.split_whitespace().next().unwrap_or_default();
            let output = Command::new("sh")
                .args(&["-c", &["eopkg info ", package].concat()])
                .output()
                .expect("failed to execute process");
            for line in str::from_utf8(&output.stdout).unwrap_or_default().lines() {
                if line.trim().is_empty() {
                    continue;
                }
                match line.split_whitespace().next().unwrap_or("Error") {
                    "Component" => {
                        let mut temp = line.split_whitespace().skip(2);
                        let component = temp.next().unwrap_or_default();
                        match component {
                            "system.base" | "desktop.core" | "xorg.driver" => break,
                            _ => continue,
                        }
                    }
                    "Reverse" => {
                        if line.len() == 22 {
                            println!("{}", package);
                        }
                    }
                    "Error" => println!("{} ❌", package),
                    _ => continue,
                }
                break;
            }
        }
    }
}
