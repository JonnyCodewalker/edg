# edg

A simple program to output installed apps that have no reverse dependencies.

By default the following components are ignored:

- system.base
- desktop.core
- xorg.driver

edg outputs everything into stdout, so you probably want to redirect it into a file.
It works by parsing the output of `eopkg li` and `eopkg info`, so it is slow.

#### Not all packages listed are necessarily safe to uninstall

It is up to the user to determine which packages can be removed.


## Install
> git clone https://gitlab.com/JonnyCodewalker/edg.git

> cd edg

> cargo build --release
